import question from "./question"

class Quiz
{
    constructor()
    {
        this.category = null;
        this.questionCount = null;
        this.difficulty = null;
        this.type = null;
        this.questions = [];
    }
    async getQuestions (category, difficulty, type, count)
    {
        let self = this;
        return new Promise(function (resolve, reject) {
            self.category = (category) ? category : 9;
            self.difficulty = (difficulty) ? difficulty : "easy";
            self.type = (type) ? type : "multiple";
            self.questionCount = (count) ? count : 10;
            fetch('https://opentdb.com/api.php?amount=' + Number(self.questionCount) +
                '&category=' + Number(self.category) +
                '&difficulty=' + self.difficulty +
                '&type=' + String(self.type) +
                '&encode=url3986')
                .then(result => result.json())
                .then(json => {
                    let index = 1;
                    json.results.forEach(q => {
                        let fixedIncorrectAnswers = [];
                        for (const quest of q.incorrect_answers)
                            fixedIncorrectAnswers.push(decodeURIComponent(quest));

                        self.questions.push(new question(decodeURIComponent(q.question), decodeURIComponent(q.correct_answer), fixedIncorrectAnswers, index));
                        index++;
                    });
                    resolve();
                })
                .catch(err => {
                    reject(new Error("no data"));
                    console.error(err.message)
                });
        });
    }
}

export default Quiz;
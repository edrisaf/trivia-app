class Question
{
    constructor(question, correctAnswer, incorrectAnswers, index)
    {
        this.question = question;
        this.correctAnswer = correctAnswer;
        this.incorrectAnswers = incorrectAnswers;
        this.index = index;
    }
}

export default Question;
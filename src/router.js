import VueRouter from 'vue-router'
import Home from './views/Home.vue'
import Questions from './views/Questions.vue'
import Results from "@/views/Results";
import {store} from "@/store";

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/questions',
        name: 'Questions',
        component: Questions,
        async beforeEnter(to, from, next)
        {
            try {
                if (store.state.quiz.questions.length > 0) {
                    next();
                }
            } catch (e) {
                next('/');
            }
        }
    },
    {
        path: '/results',
        name: 'Results',
        component: Results,
        async beforeEnter(to, from, next)
        {
            try {
                if (store.state.quiz.questions.length > 0) {
                    next();
                }
            } catch (e) {
                next('/');
            }
        }
    }
]

const router = new VueRouter({
    routes,
    mode: "history"
})

export default router

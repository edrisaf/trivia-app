# trivia-game
A simple web application where you can play a trivia game. The user can adjust the trivia settings before starting. Written in Vue.js, by Edris Afzali and Sultan Iljasov.

# How to play
The application can be accessed through [this](https://edris-sultan-trivia-game.herokuapp.com/) link or by running the server locally.
